document.querySelector('#btnprovincia').addEventListener('click',function(){
    obtProvinvia();
});

document.querySelector('#btncanton').addEventListener('click',function(){
    obtCanton();
});

document.querySelector('#btndistrito').addEventListener('click',function(){
    obtDistrito();
});


document.querySelector('#btnprovincia2').addEventListener('click',function(){
    obtProvinvia2();
});

document.querySelector('#btncanton2').addEventListener('click',function(){
    obtCanton2();
});

document.querySelector('#btndistrito2').addEventListener('click',function(){
    obtDistrito2();
});



function obtProvinvia(){
    var webProvincia='https://ubicaciones.paginasweb.cr/provincias.json';
    traerDatos(webProvincia,'provincia');    
}

function obtCanton(){
    var idProvincia=document.getElementById('provincia').value;
    var webCanton='https://ubicaciones.paginasweb.cr/provincia/'+idProvincia+'/cantones.json'

    traerDatos(webCanton,'canton');    
}
function obtDistrito(){
    var idProvincia=document.getElementById('provincia').value;
    var idCanton=document.getElementById('canton').value;
    var webDistrito='https://ubicaciones.paginasweb.cr/provincia/'+idProvincia+'/canton/'+idCanton+'/distritos.json'
    traerDatos(webDistrito,'distrito');    
}

function obtProvinvia2(){
    var webProvincia='https://ubicaciones.paginasweb.cr/provincias.json';
    traerDatos(webProvincia,'provincia2');    
}

function obtCanton2(){
    var idProvincia2=document.getElementById('provincia2').value;
    var webCanton='https://ubicaciones.paginasweb.cr/provincia/'+idProvincia2+'/cantones.json'

    traerDatos(webCanton,'canton2');    
}
function obtDistrito2(){
    var idProvincia2=document.getElementById('provincia2').value;
    var idCanton2=document.getElementById('canton2').value;
    var webDistrito='https://ubicaciones.paginasweb.cr/provincia/'+idProvincia2+'/canton2/'+idCanton2+'/distritos.json'
    traerDatos(webDistrito,'distrito2');    
}

function traerDatos(purl,ptag){
    fetch(purl)
    .then((respuesta)=>{
        return respuesta.json();
    }).then((respuesta)=>{
        Limpiar(ptag);
        let resultado= document.querySelector('#'+ptag);
            for (var clave in respuesta){
                if (respuesta.hasOwnProperty(clave)) {
                    resultado.innerHTML+="<option value='"+clave+"'>"+respuesta[clave]+"</option>";
                        
                }
            }

    })
}


function Limpiar(tag)
{
    document.getElementById(tag).innerHTML = "";
}

